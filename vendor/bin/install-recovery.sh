#!/vendor/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:83886080:8cd3a4f35d69b9712b3135db8d7f0e3dd7731df0; then
  applypatch  \
          --patch /vendor/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:100663296:dc0c42d83d7bb69c28040ceb0a9850d5b6fb1fc4 \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:83886080:8cd3a4f35d69b9712b3135db8d7f0e3dd7731df0 && \
      log -t recovery "Installing new oppo recovery image: succeeded" && \
      setprop ro.boot.recovery.updated true || \
      log -t recovery "Installing new oppo recovery image: failed" && \
      setprop ro.boot.recovery.updated false
else
  log -t recovery "Recovery image already installed"
  setprop ro.boot.recovery.updated true
fi
